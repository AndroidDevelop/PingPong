package com.example.pingpong;

import java.io.IOException;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSCounter;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.IFont;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.adt.color.Color;

import android.util.Log;
import android.util.SparseArray;

/**
 * (c) 2010 Nicolas Gramlich (c) 2011 Zynga
 * 
 * @author Nicolas Gramlich
 * @since 11:54:51 - 03.04.2010
 */
public class MovingBallExample extends SimpleBaseGameActivity {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int CAMERA_WIDTH = 720;
	private static final int CAMERA_HEIGHT = 480;

	private static final float DEMO_VELOCITY = 100.0f;

	// ===========================================================
	// Fields
	// ===========================================================

	private ITexture mFaceTexture;
	private TiledTextureRegion mFaceTextureRegion;
	private IFont mFont;	
	static Text marcador1;
	static Text marcador2;
	static Ball ball;
	static float centerX = MovingBallExample.CAMERA_WIDTH / 2;
	static float centerY = MovingBallExample.CAMERA_HEIGHT / 2;

	//

	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public EngineOptions onCreateEngineOptions() {
		final Camera camera = new Camera(0, 0, MovingBallExample.CAMERA_WIDTH,
				MovingBallExample.CAMERA_HEIGHT);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR,
				new RatioResolutionPolicy(MovingBallExample.CAMERA_WIDTH,
						MovingBallExample.CAMERA_HEIGHT), camera);
	}

	@Override
	public void onCreateResources() throws IOException {
		//carregem la imatge per a la pilota
		this.mFaceTexture = new AssetBitmapTexture(this.getTextureManager(),
				this.getAssets(), "gfx/face_circle_tiled.png",
				TextureOptions.BILINEAR);
		this.mFaceTextureRegion = TextureRegionFactory.extractTiledFromTexture(
				this.mFaceTexture, 2, 1);
		this.mFaceTexture.load();

		//creem una tipografia que despres farem servir per mostrar els fps i els marcadors
		FontFactory.setAssetBasePath("font/");
		this.mFont = FontFactory.createFromAsset(this.getFontManager(),
				this.getTextureManager(), 256, 256, this.getAssets(),
				"Droid.ttf", 16, true, android.graphics.Color.WHITE);
		this.mFont.load();
	}

	@Override
	public Scene onCreateScene() {
		this.mEngine.registerUpdateHandler(new FPSLogger());

		// declarem la scena
		final Scene scene = new Scene();
		//afegim un background de color blau
		scene.getBackground().setColor(0.09804f, 0.6274f, 0.8784f);

		// declarem una ball i l'afegim a lescene
		ball = new Ball(centerX, centerY, this.mFaceTextureRegion,
				this.getVertexBufferObjectManager());

		scene.attachChild(ball);

		final VertexBufferObjectManager vbo = this
				.getVertexBufferObjectManager();
		final ITexture scoreFontTexture = new BitmapTextureAtlas(
				this.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
		// --------- FPS---------/
		final FPSCounter fpsCounter = new FPSCounter();
		this.mEngine.registerUpdateHandler(fpsCounter);

		final Text fpsText = new Text(this.CAMERA_HEIGHT / 5,
				this.CAMERA_WIDTH / 95, this.mFont, "FPS:",
				"FPS: XXXXXXXXXX".length(), vbo);
		scene.attachChild(fpsText);

		// actulitzem el fps
		scene.registerUpdateHandler(new TimerHandler(1 / 20.0f, true,
				new ITimerCallback() {
					@Override
					public void onTimePassed(final TimerHandler pTimerHandler) {
						fpsText.setText("FPS: " + fpsCounter.getFPS());
					}
				}));

		// ---------marcadores-----------------
		marcador1 = new Text(340, this.CAMERA_HEIGHT - 10, this.mFont, "0", vbo);
		scene.attachChild(marcador1);
		marcador2 = new Text(380, this.CAMERA_HEIGHT - 10, this.mFont, "0", vbo);
		scene.attachChild(marcador2);

		// linea de en medio
		scene.attachChild(new Line(720 / 2, 480, 720 / 2, -480, vbo)); 

		// -------------------PALAS--------------

		// declarem la pala esquerra
		Rectangle paddleLeft = new Rectangle(CAMERA_WIDTH - 20,
				CAMERA_HEIGHT - 50, 15, 200, vbo) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
					final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				this.setPosition(CAMERA_WIDTH - 20, pSceneTouchEvent.getY()
						- this.getHeight() / 2);
				return true;
			}

			@Override
			protected void onManagedUpdate(float pSecondsElapsed) {
				// en el cas de que la pilota colisioni amb aquesta pala canviem
				// el moviment de la pilota
				if (ball.collidesWith(this)) {
					ball.changeBallMoviment();
				}
			}
		};

		// declarem la pala dreta
		final Rectangle paddleRight = new Rectangle(0 + 20, 0 + 50, 15, 200,
				vbo) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
					final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				this.setPosition(this.getWidth(), pSceneTouchEvent.getY()
						- this.getHeight() / 2);
				return true;
			}

			@Override
			protected void onManagedUpdate(float pSecondsElapsed) {
				// en el cas de que la pilota colisioni amb aquesta pala canviem
				// el moviment de la pilota
				if (ball.collidesWith(this)) {
					ball.changeBallMoviment();
				}
			}
		};

		// Afegim les dos pales a la escena
		scene.registerTouchArea(paddleLeft);
		scene.setTouchAreaBindingOnActionDownEnabled(true);
		scene.attachChild(paddleLeft);

		scene.registerTouchArea(paddleRight);
		scene.setTouchAreaBindingOnActionDownEnabled(true);
		scene.attachChild(paddleRight);

		return scene;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private static class Ball extends AnimatedSprite {
		private final PhysicsHandler mPhysicsHandler;

		public Ball(final float pX, final float pY,
				final TiledTextureRegion pTextureRegion,
				final VertexBufferObjectManager pVertexBufferObjectManager) {
			super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
			this.mPhysicsHandler = new PhysicsHandler(this);
			this.registerUpdateHandler(this.mPhysicsHandler);
			this.mPhysicsHandler.setVelocity(MovingBallExample.DEMO_VELOCITY,
					MovingBallExample.DEMO_VELOCITY);
		}

		/**
		 * este metodo cambia el movimiento de la bola retornanla per la
		 * direccio contraria
		 */
		public void changeBallMoviment() {
			float vx = this.mPhysicsHandler.getVelocityX();
			float vy = this.mPhysicsHandler.getVelocityY();
			int randomNum = 0 + (int)(Math.random()*10);
			if(randomNum % 2 == 0){
			this.mPhysicsHandler.setVelocity(-vx, -vy);
			}else{
				this.mPhysicsHandler.setVelocity(-vx, +vy);	
			}
		}

		@Override
		protected void onManagedUpdate(final float pSecondsElapsed) {

			if (this.mX < (this.getWidth() * 0.5f)) {
				this.mPhysicsHandler
						.setVelocityX(MovingBallExample.DEMO_VELOCITY);
				// si la pilota toca el canto dret, fem que la pilota torni a
				// sortir des del mitg
				ball.setPosition(centerX, centerY);
				// aumentem en u el mercador de la esquerra
				int puntuacion = Integer.parseInt(marcador2.getText()
						.toString());				
				marcador2.setText(String.valueOf(++puntuacion));

			} else if (this.mX + (this.getWidth() * 0.5f) > MovingBallExample.CAMERA_WIDTH) {
				this.mPhysicsHandler
						.setVelocityX(-MovingBallExample.DEMO_VELOCITY);
				// si la pilota toca el canto esquerra, fem quela pilota torni a
				// sortir des del mitg
				ball.setPosition(centerX, centerY);
				// aumentem en u el mercador de la dreta
				int puntuacion = Integer.parseInt(marcador1.getText()
						.toString());				
				marcador1.setText(String.valueOf(++puntuacion));
			}

			if (this.mY < (this.getHeight() * 0.5f)) {
				this.mPhysicsHandler
						.setVelocityY(MovingBallExample.DEMO_VELOCITY);
				// si la pilota toca la paret inferrior canviem el color de la
				// pilota per color blau
				ball.setColor(Color.BLUE);
			} else if (this.mY + (this.getHeight() * 0.5f) > MovingBallExample.CAMERA_HEIGHT) {
				this.mPhysicsHandler
						.setVelocityY(-MovingBallExample.DEMO_VELOCITY);
				// si la pilota toca la paret inferrior canviem el color de la
				// pilota per color groc
				ball.setColor(Color.YELLOW);
			}

			super.onManagedUpdate(pSecondsElapsed);
		}
	}
}
